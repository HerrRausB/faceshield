# Face Shield

Yet another face shield holder inspired by https://www.thingiverse.com/thing:4233193. I redesigned the entire thing aiming for more stability. 

# Customizing and assembly

You can open the Fusion 360 Model at https://a360.co/3aC6XjV or alternatively use the f3d export file in this repository.

Using Fusion 360 this version of the face shield also is highly customizable - especially through the parameter HeadDiameter.

I printed the part as well in PETG as an PLA - slicing and printing parameters depend on your respective machine and experience. Print time here is around 2:45h using an I3 clone at an avarage speed of 40-50mm/s and a layer height of 0.3mm.

As face shield I used a usual lamination slide, which I laminated empty. It seems to be quite stable. The slide then is fit into the slit around the outer perimeter of the holder. The slit internally is slightly concave so the slide should be held in place properly.

The holder is designed to be attached to the head using common 20mm wide rubber band for sewing - you can modify the respective width paramters in Fusion 360.

# Pre rendered STL files

I rendered STL files for head sizes from 120mm through 160mm in 5mm steps for convenient use and those who don't have access to Fusion 360.

# Stay healthy and stay home! :-)

And don't use this model for commercial purposes! Otherwise you immediately will lose face and this shield wont protect you from social damage!



